# docker_phalcon5

## Info

Stack PHP Phalcon framework v5
- nginx: 1.17.8
- php: php8.2-fpm
- mariadb
- cphalcon: 5.5.0
- php-dotenv