<?php

use Phalcon\Autoload\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->setNamespaces([
    'Com_rejosomanisindo_apps\Models' => APP_PATH . '/common/models/',
    'Com_rejosomanisindo_apps'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->setClasses([
    'Com_rejosomanisindo_apps\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'Com_rejosomanisindo_apps\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
