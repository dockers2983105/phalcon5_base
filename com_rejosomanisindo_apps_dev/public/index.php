<?php

try {
    $composer_path='../vendor/autoload.php';
    if(file_exists($composer_path)) {
        require '../vendor/autoload.php';

        /**
         * Environment variables
         */
        $dotenv = Dotenv\Dotenv::createImmutable( __DIR__ . '/../');
        $dotenv->load();
    } else {
        $throwables = new \Exception("composer package went to the rainbow-bridge");

        throw $throwables;
    }
} catch(\Exception $e) {
    echo "<pre>";
    echo $e->getMessage();
    echo "</pre>";
    exit(0);
}

require '../app/bootstrap_web.php';
